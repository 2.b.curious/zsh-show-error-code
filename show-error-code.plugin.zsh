_show_command_return_code_preexec() {
    _show_next_return_code=1
}

_show_command_return_code_precmd() {
    rCode="$?"
    if [ $_show_next_return_code ]; then
        if [[ "$rCode" -ne "0" ]]; then
            print -P "%F{#dc322f} \U21B3 $rCode %f"
        fi
    fi
    unset _show_next_return_code 
}

autoload -Uz add-zsh-hook

add-zsh-hook -D precmd _show_command_return_code_precmd
add-zsh-hook -D preexec _show_command_return_code_preexec

add-zsh-hook precmd _show_command_return_code_precmd
add-zsh-hook preexec _show_command_return_code_preexec

#vim: tabstop=4 shiftwidth=4 expandtab
